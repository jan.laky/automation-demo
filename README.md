[![pipeline status](https://gitlab.com/jan.laky/automation-demo/badges/master/pipeline.svg)](https://gitlab.com/jan.laky/automation-demo/-/commits/master)

# Automation Demo

Main goal of this project is to demonstrate process of creation 
automated regression test of Login feature of selected mobile application eshop
 and get me a job of course :)

**UPDATE**
Content of wiki moved here in to Readme file, so it can be accessed even 
without gitlab account.

## Tech/framework used

Programing language - Java

Build tool - Maven

Testing frameworks - Appium & TestNG

Executing API calls - Postman/Newman

Cloud solution - AWS Device Farm


## How to use?

1. Go to bookmark CI/CD  -> Pipelines
2. Run the pipeline 
3. Go through the logs if you want
4. Wait to finish the pipeline
5. See the [Test report](https://jan.laky.gitlab.io/automation-demo/)
6. Give me THE job :)

# The Story of perfect pipeline

## 0.Prologue

Scope of this project includes to create and execute simple regression test of 
login feature of selected mobile application eshop and provide a basic report.

Test creation will be done by Software Test Life Cycle methodology (STLC) with
use of BDD approach. Main goal os this project is to demonstrate process of creation
itself, so I will keep things simple.
  
And what BDD stands for? We can define BDD by two points of view. These two
are complementary to each other and mutually beneficial.

### Behavior-Driven development

- narrowly defined software development methodology
- relies on domain-specific languages such as Gherkin

### Business-Driven development

- philosophy
- business and tech teams work closely to define, specify, code and 
deliver software that exactly meets the needs of the business

## 1.Requirements analysis

To fulfill the task we need 4 basic elements:

1. Application under test (AUT)
2. Test application
3. Mobile device to run test on
4. Pipeline to execute the process 

## 2.Test plan

### 1. Application under test (AUT)

- There are some cli tool using unofficial pythop API able to download
APK file from Google Play store, but after I tried a few (gdplaydl, gcplaycli...),
I have to admit they are unreliable.
- So that I had to skip automation of this task and download it manually
- APK file is placed in mobileapp folder
- Mall.cz app is selected for the purposes of this work ;)   

### 2. Test application

- Technical components of test application are mentioned above
- BaseTest class is responsible for test setup: diver, waitDriver initialization, capabilities setup
- BasePageObject class is parent to all page objects (PO), its responsible for decorating mobile elements
- Page object model is used. Each PO corresponds to actual screen in AUT. Each PO has its own widgetRoot class
- WidgetRoot is a wrapper for Appium widgets (appium own built-in extension of PO model)
- Each widget represents particular UI element such a bottom menu, product list, toolbar etc.

### 3. Mobile device to run test on

- Mobile device is provided by AWS Device farm (DF) cloud service
- DF provides three types of access: UI console, cli tool, API
- In this project DF is controlled with use of API calls

### 4. Pipeline to execute the process

- Gitlab CI/CD tool is used to run sequence of jobs
- API calls are managed by newman cli tool which runs predefined postman collections

## 3.Test Case Development

- Gherkin uses a set of special keywords to give structure and meaning to executable specifications
- FEATURE: high-level description of a software feature
- SCENARIO: concrete use of feature
- GIVEN: precondition to the test
- WHEN: user action
- THEN: outcome of user action
- SCENARIO OUTLINE: run the same Scenario multiple times, with different combinations of values


**Preconditions**
- User with following credentials was registered - email: automationd666@gmail.com; password: LetsHireThisGuy#1
- Unregistered credentials: abcd@example.org; password: 12345
- Tests are written by user perspective

FEATURE: Login feature
&nbsp;


SCENARIO_1: Successful login with registered user

GIVEN: I am on HomePage

WHEN: I click on MyMall icon in bottom menu

THEN: I am on Client Center Page

WHEN: I click on Login button in toolbar

THEN: I am on Login Page

WHEN: I fill in "email"

AND: I fill in "password"

AND: I click on login button

THEN: I am on Client Center Page

AND: My name is displayed in toolbar


&nbsp;


SCENARIO_2: Unsuccessful login with unregistered user

GIVEN: I am on HomePage

WHEN: I click on MyMall icon in bottom menu

THEN: I am on Client Center Page

WHEN: I click on Login button in toolbar

THEN: I am on Login Page

WHEN: I fill in "email"

AND: I fill in "password"

AND: I click on login button

THEN: I am on Login Page

AND: Warning is displayed

&nbsp;

SCENARIO OUTLINE: "email"; "password"

&nbsp;

EXAMPLE:
&nbsp;

|       email              |     password      | Successful login |
|:-------------------------|:-----------------:|-----------------:|
| automationd666@gmail.com | LetsHireThisGuy#1 | Y |
| abcd@example.org | 12345 | N |
| automationd666@gmail.com | 12345 | N |
| abcd@example.org | LetsHireThisGuy#1 | N |


&nbsp;

- Just for fun I added another semi-test which just prints some performance data: 
memoryinfo, batteryinfo, networkinfo, cpuinfo

## 4.Test Case Execution
- Gitlab CI/CD pipeline consist of 5 stages with 6 jobs:

### 1. package

- executes maven package goal and creates zip-with-dependencies.zip artifact
with packaged tests

### 2. scheduleTestRun

- upload zip file form previous stage, APK file and aws_spec.yaml (config of aws test environment) 
file to DF and run test 

### 3. processReports

- delayed y 6 minutes
- collects basic information of test run including urls of created DF artifacts

### 4. collectReports

- download DF artifacts

### 5. pages

- copy all artifacts to public folder
- creates simple html page with test reports

### 6. deploy

- triggered manually
- deploy packaged project to Gitlabs maven package registry to be used by other projects

## 5.Defect Tracking
- out of scope

## 6.Test Execution Report
- report is published and hosted with use of Gitlab Pages

## 7.Test Closure
- analyze to find out the defect distribution by type and severity

## 8. Epilogue

Possible future improvements:
- implement Spring framework and java generics so can create any PO, now we have to know 
the type before initialization
- improve test logging, implemented logging with use of aspects, actually not working on DF 
(need further investigation)
- implement full - fledged BDD framework like cucumber
- implement AWS DF SDK into application itself

**Hope you like this work** 

**Have a nice day and good health during these difficult days**

**See ya**