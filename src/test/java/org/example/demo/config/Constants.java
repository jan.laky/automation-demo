package org.example.demo.config;

public class Constants {

    public static final String AUTOMATION_NAME_VALUE = "UiAutomator2";
    public static final String APP_PACKAGE_VALUE = "com.nrholding.mall";
    public static final String APP_ACTIVITY_VALUE = "com.zentity.mall.mvp.activity.launch.LaunchActivity";
    public static final String APP_WAIT_ACTIVITY = "com.zentity.mall.mvp.activity.main.MainActivity";

    public final static int WAIT_DRIVER_TIMEOUT = 50;
    public final static int DECORATOR_TIMEOUT = 50;
    public final static int POLLING = 200;

    public static final String DEVICE_NAME_VALUE = "CQ3000HAYU";
    public static final String APP_PACKAGE_ID = "com.nrholding.mall:id/";

}
