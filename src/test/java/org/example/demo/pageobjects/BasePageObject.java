package org.example.demo.pageobjects;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.lang.NonNull;

import java.time.Duration;

import static org.example.demo.config.Constants.*;


/**
 * A base for all the pages
 */
public abstract class BasePageObject {

    public AndroidDriver<MobileElement> driver;
    public WebDriverWait wait;


    /**
     * A base constructor that sets the page's driver
     *
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     *
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the BeforeSuite method.
     * @param wait the selenium webDriverWait created in the BeforeSuite method.
     */
    public BasePageObject(@NonNull AndroidDriver<MobileElement> driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;

        Object widgetRoot = getWidgetRoot();
        if (widgetRoot != null) {
            PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(DECORATOR_TIMEOUT)), widgetRoot);
        }
    }

    protected Object getWidgetRoot() {
        return null;
    }

    public void insertText(MobileElement input, String text) {
        wait.until(ExpectedConditions.elementToBeClickable(input));
        input.clear();
        input.findElement(MobileBy.id(APP_PACKAGE_ID + "textInput")).sendKeys(text);
    }
}
