package org.example.demo.pageobjects.widgets;

import io.appium.java_client.MobileElement;
import org.example.demo.pageobjects.widgets.subwidgets.ToolbarWidget;

public class ClientCenterWidgetRoot {

    private ToolbarWidget toolbarWidget;

    public MobileElement getActionButton() {
        return toolbarWidget.getActionBarAction();
    }

    public ToolbarWidget getToolbarWidget() {
        return toolbarWidget;
    }

    public String readHeader() {
        return toolbarWidget.getActionBarTitle().getText();
    }

}
