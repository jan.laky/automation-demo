package org.example.demo.pageobjects.widgets;

import io.appium.java_client.MobileElement;
import org.example.demo.pageobjects.widgets.subwidgets.LoginWidget;
import org.example.demo.pageobjects.widgets.subwidgets.MessageViewWidget;

public class LoginPageWidgetRoot {

    private LoginWidget loginWidget;
    private MessageViewWidget messageViewWidget;

    public MobileElement getInputEmail() {
        return loginWidget.getInputEmail();
    }

    public MobileElement getInputPassword() {
        return loginWidget.getInputPassword();
    }

    public MobileElement getButtonLogin() {
        return loginWidget.getButtonLogin();
    }

    public MessageViewWidget getMessageViewWidget() {
        return messageViewWidget;
    }

    public String getMessageText() {
        return messageViewWidget.getTextTitleElement().getText();
    }

    public MobileElement getButtonClose() {
       return messageViewWidget.getButtonClose();
    }
}
