package org.example.demo.pageobjects.widgets.subwidgets;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.WebElement;

import static org.example.demo.config.Constants.APP_PACKAGE_ID;

@AndroidFindBy(id = APP_PACKAGE_ID + "messageView")
public class MessageViewWidget extends Widget {

    @AndroidFindBy(id = APP_PACKAGE_ID + "textTitle")
    private MobileElement textMessage;

    @AndroidFindBy(id = APP_PACKAGE_ID + "buttonClose")
    private MobileElement buttonClose;

    public MessageViewWidget(WebElement element) {
        super(element);
    }

    public MobileElement getTextTitleElement() {
        return textMessage;
    }

    public MobileElement getButtonClose() {
        return buttonClose;
    }
}
