package org.example.demo.pageobjects.widgets;

import io.appium.java_client.MobileElement;
import org.example.demo.pageobjects.widgets.subwidgets.MainMenuWidget;

public class HomePageWidgetRoot {

    private MainMenuWidget mainMenuWidget;

    public MobileElement getClientCenterButton() {
        return mainMenuWidget.getMenuMall();
    }
}
