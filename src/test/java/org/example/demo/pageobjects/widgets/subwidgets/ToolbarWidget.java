package org.example.demo.pageobjects.widgets.subwidgets;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.WebElement;

import static org.example.demo.config.Constants.APP_PACKAGE_ID;

@AndroidFindBy(id = APP_PACKAGE_ID + "toolbar")
public class ToolbarWidget extends Widget {

    @AndroidFindBy(id = APP_PACKAGE_ID + "actionBarTitle")
    private MobileElement actionBarTitle;

    @AndroidFindBy(id = APP_PACKAGE_ID + "actionBarAction")
    private MobileElement actionBarAction;

    public ToolbarWidget(WebElement element) {
        super(element);
    }

    public MobileElement getActionBarTitle() {
        return actionBarTitle;
    }

    public MobileElement getActionBarAction() {
        return actionBarAction;
    }
}
