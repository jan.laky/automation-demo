package org.example.demo.pageobjects.widgets.subwidgets;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.WebElement;

import static org.example.demo.config.Constants.APP_PACKAGE_ID;

@AndroidFindBy(id = APP_PACKAGE_ID + "content")
public class LoginWidget extends Widget {

    @AndroidFindBy(id = APP_PACKAGE_ID + "inputEmail")
    private MobileElement inputEmail;

    @AndroidFindBy(id = APP_PACKAGE_ID + "inputPassword")
    private MobileElement inputPassword;

    @AndroidFindBy(id = APP_PACKAGE_ID + "buttonLogin")
    private MobileElement buttonLogin;

    public LoginWidget(WebElement element) {
        super(element);
    }

    public MobileElement getInputEmail() {
        return inputEmail;
    }

    public MobileElement getInputPassword() {
        return inputPassword;
    }

    public MobileElement getButtonLogin() {
        return buttonLogin;
    }
}
