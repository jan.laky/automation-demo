package org.example.demo.pageobjects.widgets.subwidgets;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.WebElement;

import static org.example.demo.config.Constants.APP_PACKAGE_ID;

@AndroidFindBy(id = APP_PACKAGE_ID + "sharedBottomMenu")
public class MainMenuWidget extends Widget {

    @AndroidFindBy(id = APP_PACKAGE_ID + "menuMall")
    private MobileElement menuMall;

    public MainMenuWidget(WebElement element) {
        super(element);
    }

    public MobileElement getMenuMall() {
        return menuMall;
    }
}
