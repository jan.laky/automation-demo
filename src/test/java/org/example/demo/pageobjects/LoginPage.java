package org.example.demo.pageobjects;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;
import org.example.demo.Utils.Customer;
import org.example.demo.pageobjects.widgets.LoginPageWidgetRoot;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.lang.NonNull;
import org.testng.Assert;

public class LoginPage extends BasePageObject {

    private static final LoginPageWidgetRoot widget = new LoginPageWidgetRoot();

    public LoginPage(@NonNull AndroidDriver<MobileElement> driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    protected Object getWidgetRoot() {
        return widget;
    }

    @Step("Login and redirect to client center page")
    public ClientCenterPage login(Customer customer) {
        insertText(widget.getInputEmail(), customer.getEmail());
        insertText(widget.getInputPassword(), customer.getPassword());
        wait.until(ExpectedConditions.elementToBeClickable(widget.getButtonLogin())).click();
        return new ClientCenterPage(driver, wait);
    }

    @Step("Try login and stay on login page")
    public LoginPage loginFailed(Customer customer) {
        insertText(widget.getInputEmail(), customer.getEmail());
        insertText(widget.getInputPassword(), customer.getPassword());
        wait.until(ExpectedConditions.elementToBeClickable(widget.getButtonLogin())).click();
        return this;
    }

    @Step("Close info message")
    public LoginPage closeMessage() {
        wait.until(ExpectedConditions.elementToBeClickable(widget.getButtonClose())).click();
        return this;
    }

    @Step("Assert message is displayed")
    public LoginPage assertMessageDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(widget.getMessageViewWidget().getWrappedElement()));
        Assert.assertTrue(widget.getMessageViewWidget().getWrappedElement().isDisplayed());
        return this;
    }

    @Step("Assert message is not displayed")
    public LoginPage assertMessageClosed() {
        boolean thrown = false;

        try {
            driver.findElement(MobileBy.id(widget.getMessageViewWidget().getTextTitleElement().getId()));
        } catch (NoSuchElementException e) {
            thrown = true;
        }
        Assert.assertTrue(thrown);
        return this;
    }

    @Step("Assert text message equals expected string")
    public LoginPage assertMessageText(String textExpected) {
        Assert.assertEquals(widget.getMessageText(), textExpected);
        return this;
    }
}
