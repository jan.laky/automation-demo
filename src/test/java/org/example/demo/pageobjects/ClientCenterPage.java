package org.example.demo.pageobjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;
import org.example.demo.pageobjects.widgets.ClientCenterWidgetRoot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.lang.NonNull;
import org.testng.Assert;

public class ClientCenterPage extends BasePageObject {

    private static final ClientCenterWidgetRoot widget = new ClientCenterWidgetRoot();

    public ClientCenterPage(@NonNull AndroidDriver<MobileElement> driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    protected Object getWidgetRoot() {
        return widget;
    }

    @Step("Go to login page")
    public LoginPage goToLogin() {
        wait.until(ExpectedConditions.elementToBeClickable(widget.getActionButton())).click();
        return new LoginPage(driver, wait);
    }

    @Step("Assert client center title equals expected text {0}")
    public ClientCenterPage assertClientCenterHeader(String textExpected) {
        wait.until(ExpectedConditions.textToBePresentInElement(widget.getToolbarWidget().getActionBarTitle(), textExpected));
        Assert.assertEquals(widget.readHeader(), textExpected);
        return this;
    }

}
