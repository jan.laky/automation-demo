package org.example.demo.pageobjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.qameta.allure.Step;
import org.example.demo.pageobjects.widgets.HomePageWidgetRoot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.lang.NonNull;

import java.util.HashMap;
import java.util.List;

import static org.example.demo.config.Constants.APP_PACKAGE_VALUE;

public class HomePage extends BasePageObject {

    private static final HomePageWidgetRoot widget = new HomePageWidgetRoot();

    public HomePage(@NonNull AndroidDriver<MobileElement> driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Override
    protected Object getWidgetRoot() {
        return widget;
    }

    @Step("Go to client center page")
    public ClientCenterPage goToClientCenter() {
        wait.until(ExpectedConditions.elementToBeClickable(widget.getClientCenterButton())).click();
        return new ClientCenterPage(driver, wait);
    }

}
