package org.example.demo.tests;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.net.MalformedURLException;
import java.net.URL;

import static org.example.demo.config.Constants.*;

/**
 * An abstract base for all of the tests
 *
 * Responsible for setting up the Appium test Driver, waitDriver
 * and capabilities
 */
public abstract class BaseTest {

    public static AndroidDriver<MobileElement> driver;
    public static WebDriverWait wait;
    String appiumUrl = "http://127.0.0.1:4723/wd/hub";


    /**
     * Appium server is started outside of thi application
     * and running on http://127.0.0.1:4723/wd/hub address.
     *
     * For local run replace the url with startAppiumServer()
     * method
     *
     * @throws MalformedURLException
     */
    @BeforeSuite
    public void setup() throws MalformedURLException {
        driver = new AndroidDriver<>(new URL(appiumUrl), capabilities());
        wait = new WebDriverWait(driver, WAIT_DRIVER_TIMEOUT, POLLING);
    }

    @AfterSuite
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
        public void beforeMethod() {
        System.out.println(driver.getCapabilities());
    }

    @AfterMethod
    public void reset() {
        driver.resetApp();
    }


    /**
     * Use for local run
     *
     * @return URL of running appium server
     */
    private static URL startAppiumServer() {
        final AppiumServiceBuilder serviceBuilder = new AppiumServiceBuilder().usingAnyFreePort();
        final AppiumDriverLocalService server = AppiumDriverLocalService.buildService(serviceBuilder);
        server.start();

        if (!server.isRunning()) {
            throw new AppiumServerHasNotBeenStartedLocallyException(
                    "An appium server node has not been started!");
        }
        return server.getUrl();
    }

    /**
     * For local run uncomment device name capability and set
     * ID of your phone in Constants class
     *
     * @return
     */
    private DesiredCapabilities capabilities() {
        final DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME_VALUE);
//        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME_VALUE);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, APP_PACKAGE_VALUE);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, APP_ACTIVITY_VALUE);
        capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, APP_WAIT_ACTIVITY);
        capabilities.setCapability(AndroidMobileCapabilityType.IGNORE_UNIMPORTANT_VIEWS, true);
        return capabilities;
    }
}
