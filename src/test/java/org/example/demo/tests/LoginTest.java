package org.example.demo.tests;

import org.example.demo.Utils.Customer;
import org.example.demo.pageobjects.HomePage;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    Customer customer_registered = new Customer("automationd666@gmail.com", "LetsHireThisGuy#1");
    Customer customer_unregistered = new Customer("abcd@example.org", "12345");
    String fullName = "Automation Demo";
    String warningText = "Přihlašovací údaje nejsou platné";

    @Test(priority = 0, description = "Valid login scenario with registered username and password")
    public void whenCredentialsAreValid_thenLoginIsSuccessfulOnMyMallPage() throws InterruptedException {

        new HomePage(driver, wait)
                .goToClientCenter()
                .goToLogin()
                .login(customer_registered)
                .assertClientCenterHeader(fullName);
    }

    @Test(priority = 0, description = "Invalid login scenario with unregistered username and password")
    public void whenCredentialsAreInValid_thenWarningIsDisplayed() throws InterruptedException {

        new HomePage(driver, wait)
                .goToClientCenter()
                .goToLogin()
                .loginFailed(customer_unregistered)
                .assertMessageDisplayed()
                .assertMessageText(warningText)
                .closeMessage()
                .assertMessageClosed();
    }
}
