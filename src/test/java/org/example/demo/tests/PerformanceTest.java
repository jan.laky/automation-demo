package org.example.demo.tests;

import org.example.demo.pageobjects.HomePage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;

import static org.example.demo.config.Constants.APP_PACKAGE_VALUE;

public class PerformanceTest extends BaseTest {

    @DataProvider(name = "performanceDataTypes")
    public Object[][] dataProviderMethod() {
        return new Object[][] {{"memoryinfo"}, {"batteryinfo"}, {"networkinfo"}, {"cpuinfo"} };
    }

    @Test(priority = 0, dataProvider = "performanceDataTypes", description = "Print performance data")
    public void performanceData(String dataType) {

        List<List<Object>> data = driver.getPerformanceData(APP_PACKAGE_VALUE, dataType, 50);
        HashMap<String, Integer> readableData = new HashMap<>();
        for (int i = 0; i < data.get(0).size(); i++) {
            int val;
            if (data.get(1).get(i) == null) {
                val = 0;
            } else {
                val = Integer.parseInt((String) data.get(1).get(i));
            }
            readableData.put((String) data.get(0).get(i), val);
        }
        System.out.println(readableData);
    }
}
