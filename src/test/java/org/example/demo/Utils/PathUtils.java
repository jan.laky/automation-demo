package org.example.demo.Utils;

import org.springframework.lang.NonNull;

import java.io.File;

public class PathUtils {

    private static final String ANDROID_APP_NAME = "mall-cz_20.08.17.apk";

    private PathUtils() {
        throw new UnsupportedOperationException();
    }

    public static String getAndroidAppPath() {
        return getAppPath(ANDROID_APP_NAME);
    }

    private static final String getAppPath(@NonNull final String appName) {
        final File classPathRoot = new File(System.getProperty("user.dir"));
        final File applicationDirectory = new File(classPathRoot, "src/test/resources/apps/");
        final File application = new File(applicationDirectory, appName);
        return application.getAbsolutePath();
    }
}
