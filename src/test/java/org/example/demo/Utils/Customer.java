package org.example.demo.Utils;

public class Customer {

    private final String email;
    private final String password;

    public Customer(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {

        return password;
    }
}
